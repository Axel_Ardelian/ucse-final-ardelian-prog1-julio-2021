﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Logica.Envio;
using static Logica.Historial;

namespace Logica
{
    public class Principal
    {
        public List<Envio> Envios { get; set; }
        public List<Repartidor> Repartidores { get; set; }
        public int CargarEnvio(int dniDestinatario, DateTime fechaEstimada, string descripcion)
        {
            Envio nuevoEnvio = new Envio();
            nuevoEnvio.NumeroAutoincremental += 1;
            nuevoEnvio.DNIDestinatario = dniDestinatario;
            nuevoEnvio.Eventos = Tipo.Llegada_Al_Centro_De_Distribucion;
            nuevoEnvio.Historial.Estado = TipoEstado.Pendiente_Envio;
            nuevoEnvio.Historial.FechaDelEvento = DateTime.Today;
            nuevoEnvio.Descripcion = descripcion;
            nuevoEnvio.DNIRepartidor = 0;
            nuevoEnvio.FechaEstimadaDeEntrega = fechaEstimada;
            return nuevoEnvio.NumeroAutoincremental;
        }
        public Mensaje ActualizarEstadoDelEnvio(int numeroDeEnvio, Tipo evento, int codigoPostal, Repartidor repartidor)
        {
            Mensaje mensaje = new Mensaje();
            Envio envioActualizado = Envios.Find(x => x.NumeroAutoincremental == numeroDeEnvio);
            if (envioActualizado != null)
            {
                if (envioActualizado.Historial.Validacion(envioActualizado.Historial.Estado,envioActualizado))
                {
                    envioActualizado.Eventos = evento;
                    if (evento == Tipo.En_Viaje)
                        envioActualizado.Historial.Estado = TipoEstado.Enviado;
                    else
                    {
                        if (evento == Tipo.En_Manos_Del_Repartidor)
                        {
                            envioActualizado.Historial.Estado = TipoEstado.Ultimo_Tramo_Del_Recorrido;
                            envioActualizado.DNIRepartidor = AsignarRepartidor(repartidor, codigoPostal);
                        }
                           
                        else
                        {
                            if (evento == Tipo.Entregado)
                            {
                                envioActualizado.Historial.Estado = TipoEstado.Entregado;
                                envioActualizado.Historial.FechaDelEvento = DateTime.Today;
                            }
                        }
                    }
                    mensaje.Estado = true;
                    mensaje.Mensaje = "Se actualizo correctamente";
                    return mensaje;                   
                }
                else
                {
                    mensaje.Estado = false;
                    mensaje.Mensaje = "Error";
                    return mensaje;
                }          
            }
            mensaje.Estado = false;
            mensaje.Mensaje = "Error";
            return mensaje;
        }
        public int AsignarRepartidor(Repartidor repartidor, int codigoPostal)
        {
            Envio envio = Envios.Find(x => x.DNIRepartidor == 0);
            if (envio != null)
            {
                if (repartidor.CodigosPostales.Exists(x=>x == codigoPostal))
                {
                    return envio.DNIRepartidor = repartidor.Dni;
                    
                }
                
            }
            return 0;
        }
    }
}
