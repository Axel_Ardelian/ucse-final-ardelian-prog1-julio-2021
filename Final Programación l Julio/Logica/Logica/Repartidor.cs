﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Repartidor
    {
        public int Dni { get; set; }
        public string NombreYApellido { get; set; }
        public int TelefonoDeContacto { get; set; }
        public List<int> CodigosPostales { get; set; }
        
    }
}
