﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Envio
    {
        public int NumeroAutoincremental { get; set; }
        public int DNIDestinatario { get; set; }
        public int DNIRepartidor { get; set; }
        public enum Tipo { Llegada_Al_Centro_De_Distribucion, En_Viaje, En_Manos_Del_Repartidor, Entregado }
        public Tipo Eventos { get; set; }
        public DateTime FechaEstimadaDeEntrega { get; set; }
        public DateTime FechaDeEntrega { get; set; }
        public Historial Historial { get; set; }
        public string Descripcion { get; set; }

    }
}
