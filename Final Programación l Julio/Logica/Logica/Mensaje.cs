﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Mensaje
    {
        public bool Estado { get; set; }
        public string Mensaje { get; set; }
    }
}
