﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Historial
    {
        public enum TipoEstado { Pendiente_Envio, Enviado, Ultimo_Tramo_Del_Recorrido, Entregado }
        public TipoEstado Estado { get; set; }
        public DateTime FechaDelEvento { get; set; }

        public bool Validacion(TipoEstado estado, Envio envio)
        {
            if (envio.Historial.Estado == TipoEstado.Pendiente_Envio && estado != TipoEstado.Enviado)
            {
                return false;
            }
                
            if (envio.Historial.Estado == TipoEstado.Enviado && estado != TipoEstado.Ultimo_Tramo_Del_Recorrido)
            {
                return false;
            }
            if (envio.Historial.Estado == TipoEstado.Ultimo_Tramo_Del_Recorrido && estado != TipoEstado.Entregado)
            {
                return false;
            }
            return true;
        }
    }
}
