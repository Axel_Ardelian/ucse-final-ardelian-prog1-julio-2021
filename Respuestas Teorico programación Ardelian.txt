1-
a- Incorrecta.  Genera un error ya que el índice estaba fuera del intervalo. 
b- Incorrecta.  Genera un error al intentar dividir porque dentro de 'valores' no se encuenta nada. 
c- Incorrecta. Value no puede ser índice de Values ya que son de distinto tipo.
d- Incorrecta. El indice, en este caso 'i' no debe ser menor a 0.

2) La expresión lambda se basa en un elemento de sintaxis y un operador que difieren de lo que ha visto en los temas anteriores. El operador es =>
una expresión lambda está compuesta por dos partes:
List(int) MayoresDe8 = Mayores.Find(x => x.Nro >  7);
El lado izquierdo especifica los parámetros requeridos por la expresión lambda.
En el lado derecho está el cuerpo lambda, que especifica las acciones de la expresión lambda.

3) public bool Suma(int resultado)
	{	
		int A = 3;
		int B = 4;
		int Suma = A + B;
		if(Suma == resultado)
			return true;
		return false;
	}

5) Un booleano con valor false. 
Justificación: No se permite el operador == ya que se utiliza solo en sentencias condicionales como por ej if ( a == b)